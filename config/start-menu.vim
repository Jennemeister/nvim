let g:startify_custom_header = []

let g:startify_lists = [
          \ { 'type': 'files',     'header': ['   Files']            },
          \ { 'type': 'bookmarks', 'header': ['   Bookmarks']      },
          \ ]


let g:startify_bookmarks = [
            \ { 'd': '~/.local/src/dwm.git/config.h' },
            \ { 'i': '~/.config/nvim/init.vim' },
            \ { 's': '~/.local/src/st/config.h' },
            \ { 'z': '~/.config/zsh/.zshrc' },
            \ '~/Code',
            \ '~/Documents',
            \ ]
