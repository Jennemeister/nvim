" Leader is mapped here for some unknown reason
let mapleader=" "

source $HOME/.config/nvim/autocompile.vim
source $HOME/.config/nvim/mappings.vim
source $HOME/.config/nvim/plugins.vim
source $HOME/.config/nvim/settings.vim
source $HOME/.config/nvim/config/floaterm.vim
source $HOME/.config/nvim/config/quickscope.vim
source $HOME/.config/nvim/config/start-menu.vim
source $HOME/.config/nvim/config/which-key.vim

" Themes
"source $HOME/.config/nvim/themes/happy_hacking.vim
source $HOME/.config/nvim/themes/onedark.vim
