" auto-install vim-plug
if empty(glob('~/.config/nvim/autoload/plug.vim'))
  silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  "autocmd VimEnter * PlugInstall
  "autocmd VimEnter * PlugInstall | source $MYVIMRC
endif

call plug#begin('~/.config/nvim/autoload/plugged')
    " lf for vim   
    Plug 'ptzz/lf.vim'
    Plug 'rbgrouleff/bclose.vim'
    " Ranger
    Plug 'kevinhwang91/rnvimr', {'do': 'make-sync'}
    " vidir - edit directory 
    Plug 'trapd00r/vidir'
    " Quick-scope
    Plug 'unblevable/quick-scope'
    " Floaterm
    Plug 'voldikss/vim-floaterm'
    " Start menu
    Plug 'mhinz/vim-startify'
    " Dynamic code critique
    Plug 'ChristianChiarulli/codi.vim'
    " Easy comments
    Plug 'tpope/vim-commentary'
    " Shortcut Overview
    Plug 'liuchengxu/vim-which-key'
    " Better Syntax Support
    Plug 'sheerun/vim-polyglot'
    " Auto pairs for '(' '[' '{'
    Plug 'jiangmiao/auto-pairs'
    " Conquest of Completion
    Plug 'neoclide/coc.nvim', {'branch': 'release'}
    " Syntax
    Plug 'HerringtonDarkholme/yats.vim'
    " Git integration
    Plug 'mhinz/vim-signify'
    Plug 'tpope/vim-fugitive'
    Plug 'tpope/vim-rhubarb'
    Plug 'junegunn/gv.vim'   
    " Rainbow 
    Plug 'junegunn/rainbow_parentheses.vim'
    " Be good at vim
    " Plug 'ThePrimeagen/vim-be-good'
    " Themes
    Plug 'joshdick/onedark.vim'
    Plug 'ayu-theme/ayu-vim'

    " Plugin collection
call plug#end()
